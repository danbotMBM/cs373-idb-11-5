# cs373-idb-11-5

## Website
https://www.soccerstars.me/

## Develop url
https://develop.d36n1ulz5li55j.amplifyapp.com/

## Canvas group
11 AM, Group 5

## Names
| Name           | EID     | GitLab ID              |
|----------------|---------|------------------------|
| Daniel Jones   | dmj2378 | danbotMBM              |
| Yazan Alsukhni | yha225  | yalsukhni              |
| Grant Shirah   | gms2872 | gmshirah               | 
| Dhruv Sethi    | ds53434 | dhruvsethi227          | 

## Project Name
soccerstars.me

## Project Leader
Grant Shirah - organized and distributed tasks

## Git SHA
f9fa8df55aa43b57ab50a367f817ac277c72c306

## Git pipelines
https://gitlab.com/danbotMBM/cs373-idb-11-5/-/pipelines

## Completion time
| Name           | Estimated Time | Actual Time |
|----------------|----------------|-------------|
| Daniel Jones   | 6 hrs          | 5 hrs       |
| Yazan Alsukhni | 8 hrs          | 7 hrs       |
| Grant Shirah   | 12 hrs         | 15 hrs      | 
| Dhruv Sethi    | 6 hrs          | 5 hrs       | 
