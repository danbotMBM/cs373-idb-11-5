import sys
import requests
import json
import os
import time

import yaml

OUTDIR = "json/"

REQS_PER_MIN = 450
TIMEOUT = 60 / REQS_PER_MIN

country_api_map = {
    "Albania": "Albania",
    "Algeria": "Algeria",
    "Andorra": "Andorra",
    "Angola": "Angola",
    "Argentina": "Argentina",
    "Armenia": "Armenia",
    "Aruba": "Aruba",
    "Australia": "Australia",
    "Austria": "Austria",
    "Azerbaijan": "Azerbaijan",
    "Bahrain": "Bahrain",
    "Bangladesh": "Bangladesh",
    "Barbados": "Barbados",
    "Belarus": "Belarus",
    "Belgium": "Belgium",
    "Belize": "Belize",
    "Benin": "Benin",
    "Bermuda": "Bermuda",
    "Bhutan": "Bhutan",
    "Bolivia": "Bolivia",
    "Bosnia": "Bosnia and Herzegovina",
    "Botswana": "Botswana",
    "Brazil": "Brazil",
    "Bulgaria": "Bulgaria",
    "Burundi": "Burundi",
    "Cambodia": "Cambodia",
    "Cameroon": "Cameroon",
    "Canada": "Canada",
    "Chile": "Chile",
    "Colombia": "Colombia",
    "Croatia": "Croatia",
    "Cuba": "Cuba",
    "Curacao": "Curaçao",
    "Cyprus": "Cyprus",
    "Denmark": "Denmark",
    "Ecuador": "Ecuador",
    "Egypt": "Egypt",
    "Estonia": "Estonia",
    "Eswatini": "Eswatini",
    "Ethiopia": "Ethiopia",
    "Fiji": "Fiji",
    "Finland": "Finland",
    "France": "France",
    "Gambia": "Gambia",
    "Germany": "Germany",
    "Ghana": "Ghana",
    "Gibraltar": "Gibraltar",
    "Greece": "Greece",
    "Guadeloupe": "Guadeloupe",
    "Guatemala": "Guatemala",
    "Haiti": "Haiti",
    "Honduras": "Honduras",
    "Hungary": "Hungary",
    "Iceland": "Iceland",
    "Indonesia": "Indonesia",
    "Iraq": "Iraq",
    "Israel": "Israel",
    "Italy": "Italy",
    "Jamaica": "Jamaica",
    "Japan": "Japan",
    "Jordan": "Jordan",
    "Kazakhstan": "Kazakhstan",
    "Kenya": "Kenya",
    "Kosovo": "Kosovo",
    "Kuwait": "Kuwait",
    "Kyrgyzstan": "Kyrgyzstan",
    "Laos": "Laos",
    "Latvia": "Latvia",
    "Lebanon": "Lebanon",
    "Liberia": "Liberia",
    "Libya": "Libya",
    "Liechtenstein": "Liechtenstein",
    "Lithuania": "Lithuania",
    "Luxembourg": "Luxembourg",
    "Macedonia": "North Macedonia",
    "Malawi": "Malawi",
    "Malaysia": "Malaysia",
    "Maldives": "Maldives",
    "Malta": "Malta",
    "Mauritania": "Mauritania",
    "Mauritius": "Mauritius",
    "Mexico": "Mexico",
    "Moldova": "Moldova",
    "Mongolia": "Mongolia",
    "Montenegro": "Montenegro",
    "Morocco": "Morocco",
    "Myanmar": "Myanmar",
    "Namibia": "Namibia",
    "Nepal": "Nepal",
    "Nicaragua": "Nicaragua",
    "Nigeria": "Nigeria",
    "Norway": "Norway",
    "Pakistan": "Pakistan",
    "Palestine": "Palestine",
    "Panama": "Panama",
    "Paraguay": "Paraguay",
    "Peru": "Peru",
    "Philippines": "Philippines",
    "Poland": "Poland",
    "Portugal": "Portugal",
    "Qatar": "Qatar",
    "Romania": "Romania",
    "Russia": "Russia",
    "Rwanda": "Rwanda",
    "Senegal": "Senegal",
    "Serbia": "Serbia",
    "Singapore": "Singapore",
    "Slovakia": "Slovakia",
    "Slovenia": "Slovenia",
    "Somalia": "Somalia",
    "Spain": "Spain",
    "Surinam": "Suriname",
    "Sweden": "Sweden",
    "Switzerland": "Switzerland",
    "Syria": "Syria",
    "Tajikistan": "Tajikistan",
    "Tanzania": "Tanzania",
    "Thailand": "Thailand",
    "Tunisia": "Tunisia",
    "Turkey": "Turkey",
    "Turkmenistan": "Turkmenistan",
    "Uganda": "Uganda",
    "Ukraine": "Ukraine",
    "Uruguay": "Uruguay",
    "USA": "United States",
    "Uzbekistan": "Uzbekistan",
    "Venezuela": "Venezuela",
    "Vietnam": "Vietnam",
    "Zambia": "Zambia",
    "Zimbabwe": "Zimbabwe",
    "China": "China",
    "Congo": "Republic of the Congo",
    "Georgia": "Georgia",
    "Guinea": "Guinea",
    "India": "India",
    "Iran": "Iran",
    "Ireland": "Ireland",
    "Mali": "Mali",
    "Netherlands": "Netherlands",
    "Oman": "Oman",
    "Sudan": "Sudan",
    "England": "United Kingdom",
    "Burkina-Faso": "Burkina Faso",
    "Chinese-Taipei": "Taiwan",
    "Congo-DR": "DR Congo",
    "Costa-Rica": "Costa Rica",
    "Czech-Republic": "Czechia",
    "Dominican-Republic": "Dominican Republic",
    "El-Salvador": "El Salvador",
    "Faroe-Islands": "Faroe Islands",
    "Hong-Kong": "Hong Kong",
    "Ivory-Coast": "Ivory Coast",
    "New-Zealand": "New Zealand",
    "Northern-Ireland": "United Kingdom",
    "San-Marino": "San Marino",
    "Saudi-Arabia": "Saudi Arabia",
    "South-Africa": "South Africa",
    "South-Korea": "South Korea",
    "Trinidad-And-Tobago": "Trinidad and Tobago",
    "United-Arab-Emirates": "United Arab Emirates",
    "Scotland": "United Kingdom",
    "Wales": "United Kingdom",
    "Crimea": "Ukraine",
}

countries = [
    "Albania",
    "Algeria",
    "Andorra",
    "Angola",
    "Argentina",
    "Armenia",
    "Aruba",
    "Australia",
    "Austria",
    "Azerbaijan",
    "Bahrain",
    "Bangladesh",
    "Barbados",
    "Belarus",
    "Belgium",
    "Belize",
    "Benin",
    "Bermuda",
    "Bhutan",
    "Bolivia",
    "Bosnia",
    "Botswana",
    "Brazil",
    "Bulgaria",
    "Burkina-Faso",
    "Burundi",
    "Cambodia",
    "Cameroon",
    "Canada",
    "Chile",
    "China",
    "Chinese-Taipei",
    "Colombia",
    "Congo",
    "Congo-DR",
    "Costa-Rica",
    "Crimea",
    "Croatia",
    "Cuba",
    "Curacao",
    "Cyprus",
    "Czech-Republic",
    "Denmark",
    "Dominican-Republic",
    "Ecuador",
    "Egypt",
    "El-Salvador",
    "England",
    "Estonia",
    "Eswatini",
    "Ethiopia",
    "Faroe-Islands",
    "Fiji",
    "Finland",
    "France",
    "Gambia",
    "Georgia",
    "Germany",
    "Ghana",
    "Gibraltar",
    "Greece",
    "Guadeloupe",
    "Guatemala",
    "Guinea",
    "Haiti",
    "Honduras",
    "Hong-Kong",
    "Hungary",
    "Iceland",
    "India",
    "Indonesia",
    "Iran",
    "Iraq",
    "Ireland",
    "Israel",
    "Italy",
    "Ivory-Coast",
    "Jamaica",
    "Japan",
    "Jordan",
    "Kazakhstan",
    "Kenya",
    "Kosovo",
    "Kuwait",
    "Kyrgyzstan",
    "Laos",
    "Latvia",
    "Lebanon",
    "Liberia",
    "Libya",
    "Liechtenstein",
    "Lithuania",
    "Luxembourg",
    "Macedonia",
    "Malawi",
    "Malaysia",
    "Maldives",
    "Mali",
    "Malta",
    "Mauritania",
    "Mauritius",
    "Mexico",
    "Moldova",
    "Mongolia",
    "Montenegro",
    "Morocco",
    "Myanmar",
    "Namibia",
    "Nepal",
    "Netherlands",
    "New-Zealand",
    "Nicaragua",
    "Nigeria",
    "Northern-Ireland",
    "Norway",
    "Oman",
    "Pakistan",
    "Palestine",
    "Panama",
    "Paraguay",
    "Peru",
    "Philippines",
    "Poland",
    "Portugal",
    "Qatar",
    "Romania",
    "Russia",
    "Rwanda",
    "San-Marino",
    "Saudi-Arabia",
    "Scotland",
    "Senegal",
    "Serbia",
    "Singapore",
    "Slovakia",
    "Slovenia",
    "Somalia",
    "South-Africa",
    "South-Korea",
    "Spain",
    "Sudan",
    "Surinam",
    "Sweden",
    "Switzerland",
    "Syria",
    "Tajikistan",
    "Tanzania",
    "Thailand",
    "Trinidad-And-Tobago",
    "Tunisia",
    "Turkey",
    "Turkmenistan",
    "Uganda",
    "Ukraine",
    "United-Arab-Emirates",
    "Uruguay",
    "USA",
    "Uzbekistan",
    "Venezuela",
    "Vietnam",
    "Wales",
    "Zambia",
    "Zimbabwe",
]
country_text = open("countrycodes.txt", "r")
news_id_map = {
    w[0].strip().replace(" ", ""): w[1].strip()
    for w in (l.split(" - ") for l in country_text.readlines())
}


def ensure_dir(path):
    try:
        os.mkdir(path)
    except FileExistsError:
        pass
    return path


ensure_dir(OUTDIR)
ensure_dir(OUTDIR + "countries/")
ensure_dir(OUTDIR + "teams/")
ensure_dir(OUTDIR + "athletes/")
ensure_dir(OUTDIR + "temp/")
ensure_dir(OUTDIR + "temp/leagues/")
ensure_dir(OUTDIR + "temp/teams_in_league")


API_Keys = json.load(open("apikeys.json"))
# print(API_Keys)
API_URLs = {
    "football-api": "https://v3.football.api-sports.io/",
    "news-api": "https://newsdata.io/api/1/news?apikey=" + API_Keys["news-api"],
    "country-api": "https://restcountries.com/v3.1/",
}


def write_json(json_obj, dir, name):
    ensure_dir(dir)
    f = open(dir + "/" + str(name) + ".json", "w")
    json.dump(json_obj, f)
    f.close()


def recursive(call, url, page=1, builder={}):
    tempurl = url
    if page != 1:
        tempurl += "&page=" + str(page)
    print(tempurl, page, builder.get("paging", {"total": 0})["total"])
    result = call(tempurl)
    j = result.json()
    if not builder:
        builder = j
    else:
        builder["response"].extend(j["response"])
        builder["paging"]["current"] = j["paging"]["current"]
    if int(j["paging"]["current"]) < int(j["paging"]["total"]) and not j["errors"]:
        time.sleep(TIMEOUT)
        builder = recursive(call, url, page + 1, builder)
    if j["errors"]:
        print(j["errors"])
        return []
    return builder


def call_country(url):
    return requests.get(API_URLs["country-api"] + url)


def call_football(url):
    return requests.request(
        "GET",
        API_URLs["football-api"] + url,
        headers={"x-apisports-key": API_Keys["football-api"]},
    )


def call_news(url):
    return requests.get(API_URLs["news-api"] + url)


# (name, id) pair
def get_news(country):
    if country in news_id_map:
        id = news_id_map[country]
        response = call_news("&country=" + id + "&language=en")
        try:
            if int(response.json()["totalResults"]) == 0:
                response = call_news("&country=" + id)
        except KeyError:
            print("News api FAILED:", response.json())
        return response.text
    return "{}"


def scrape_country_info(country):

    f = open(OUTDIR + "countries/" + country + ".json", "w")
    data = None
    try:
        response = call_country("name/" + country_api_map[country] + "?fullText=true")
        data = json.loads(response.text)
    except Exception as e:
        data = {}
        print("FAILED countryapi", country)

    if type(data) is list:
        data = data[0]
    else:
        print(
            "country api call failed:",
            country,
            data,
            country_api_map.get(country, default=None),
        )
    news_element = json.loads(get_news(country))
    foot_elem = json.loads(call_football("leagues/?country=" + country).text)
    json.dump({"stats": data, "news": news_element, "league": foot_elem}, f)
    f.close()


def scrape_team_stats(team, year):
    req = (
        "teams/statistics?season="
        + str(year)
        + "&team="
        + str(team[1])
        + "&league="
        + str(team[2])
    )
    response = recursive(call_football, req)
    response["response"]["league"] = {**response["response"]["league"], **team[3]}
    d = ensure_dir(OUTDIR + "teams/")
    f = open(d + str(team[1]) + ".json", "w")
    json.dump(response, f)


# league is a (name, id) pair
def scrape_player_stats(league, year):
    players_in_league = recursive(
        call_football, "players?league=" + str(league[1]) + "&season=" + str(year)
    )
    for p in players_in_league["response"]:
        d = ensure_dir(OUTDIR + "athletes/")
        try:
            f = open(d + str(p["player"]["id"]) + ".json", "w")
            json.dump(p, f)
            f.close()
        except Exception as e:
            print("FAILED on", p["player"]["name"], e)


def scrape_leagues(countries):
    cwithpages = []
    # find some leagues for each country
    temp = countries
    for c in temp:
        res = call_football("leagues/?country=" + c)
        f = open(OUTDIR + "/temp/leagues/" + c + ".json", "w")
        f.write(res.text)
        res = res.json()
        if res["paging"]["total"] > 1:
            cwithpages.append((c, res["paging"]["total"]))

        time.sleep(TIMEOUT)
    for c in cwithpages:
        print(*c)


# leagues is (name, id)
def get_leagues_team_list(leagues, year):
    result = []
    for k in leagues:
        d = ensure_dir(OUTDIR + "temp/teams_in_league/")
        path = d + str(k[1]) + ".json"
        if not os.path.isfile(path):
            f = open(path, "w")
            req = "teams?league=" + str(k[1]) + "&season=" + str(year)
            response = recursive(call_football, req)
            json.dump(response, f)
            result.append(f.name)
            f.close()
        else:
            result.append(path)
    return result


def choose_league(league_data, num):
    try:
        result = []
        for league in league_data:
            for s in league["seasons"]:
                cov = s["coverage"]
                if s["year"] == 2021 and cov["players"] and cov["top_scorers"]:
                    result.append((league["league"]["name"], league["league"]["id"]))
        result.sort(key=lambda x: x[1])
        return result[:num]
    except Exception as e:
        return []


# send in the path to a Country League Json and return (name, id, pairs)
def choose_leagues_from_country(path, num=1):
    f = open(path, "r")
    league_data = json.load(f)["response"]
    res = choose_league(league_data, num)
    f.close()
    return res


# returns (name, id, leaguid, extra info)
def choose_teams_from_league(path):
    f = open(path, "r")
    r = json.load(f)
    leagueid = r["parameters"]["league"]
    teams = r["response"]
    result = []
    for t in teams:
        team = t["team"]
        details = {"founded": team["founded"], "venue": t["venue"]}
        result.append((team["name"], team["id"], leagueid, details))
    return result


def full_football_scrape(countryfile):
    leagues = choose_leagues_from_country(countryfile)
    league_paths = get_leagues_team_list(leagues, 2021)
    for team_list in league_paths:
        print("Getting teams from ", team_list)
        teams = choose_teams_from_league(team_list)
        for team in teams:
            time.sleep(TIMEOUT)
            try:
                print("Getting team info", team[0])
                scrape_team_stats(team, 2021)
            except Exception as e:
                print("FAILED^^", e)
    for l in leagues:
        print("Getting players from ", l)
        scrape_player_stats(l, 2021)


if __name__ == "__main__":
    for c in countries:
        scrape_country_info(c)
        print("scraping", c)
        time.sleep(TIMEOUT)
    scrape_leagues(countries)
    for c in countries:
        print("soccer stats", c)
        full_football_scrape("json/temp/leagues/" + c + ".json")
