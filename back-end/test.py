from unittest import main, TestCase
from app import app

"""
Based off of implementation in Parkdex IDB Project

Link: https://gitlab.com/maxwthomas/cs373-idb/-/blob/phase2/backend/test.py
"""


class TestModels(TestCase):
    def test_athletes(self):
        test_client = app.test_client()
        req = test_client.get("/athletes")
        self.assertEqual(req.status_code, 200)

    def test_athletes_page_2(self):
        test_client = app.test_client()
        req = test_client.get("/athletes?page=2")
        self.assertEqual(req.status_code, 200)

    def test_athletes_id_34(self):
        test_client = app.test_client()
        req = test_client.get("/athletes/34")
        self.assertEqual(req.status_code, 200)

    def test_teams(self):
        test_client = app.test_client()
        req = test_client.get("/teams")
        self.assertEqual(req.status_code, 200)

    def test_teams_page_2(self):
        test_client = app.test_client()
        req = test_client.get("/teams?page=2")
        self.assertEqual(req.status_code, 200)

    def test_teams_id_118(self):
        test_client = app.test_client()
        req = test_client.get("/teams/118")
        self.assertEqual(req.status_code, 200)

    def test_countries(self):
        test_client = app.test_client()
        req = test_client.get("/countries")
        self.assertEqual(req.status_code, 200)

    def test_countries_page_2(self):
        test_client = app.test_client()
        req = test_client.get("/countries?page=2")
        self.assertEqual(req.status_code, 200)

    def test_countries_name_argentina(self):
        test_client = app.test_client()
        req = test_client.get("/countries/Argentina")
        self.assertEqual(req.status_code, 200)

    def test_countries_name_australia(self):
        test_client = app.test_client()
        req = test_client.get("/countries/Australia")
        self.assertEqual(req.status_code, 200)


if __name__ == "__main__":
    main()
