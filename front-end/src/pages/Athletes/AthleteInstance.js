import React, { useState, useEffect } from "react";
import axios from 'axios';
import { Row, Col, Container, Card, CardBody, CardImg, Button } from 'reactstrap';
import { Tabs } from "react-bootstrap";

function AthleteInstance() {

    const athletesAPI = `https://api.soccerstars.me${window.location.pathname}`;

    const [athleteData, setAthleteData] = useState([]);
    const [loading, setLoading] = useState(false);
    const [tryAgain, setTryAgain] = useState(false);

    useEffect(() => {
        setLoading(true);
        // let isSubscribed = true
        // console.log(isSortAscending)
        // if(isSortAscending) {
        //     setDesc(null)
        // } else {
        //     setDesc(true)
        // }
        axios.get(athletesAPI)
            .then((response) => {
                setAthleteData(response.data);
            })
            .catch(function (error) {
                console.error(error);
            })
            .then(function () {
                setLoading(false);
                setTryAgain(false);
            });
        // return () => isSubscribed = false
    }, [athletesAPI, tryAgain]);

    useEffect(() => {
        document.title = `Soccer Stars | Athletes | ${athleteData.name}`;
    });

    if (!athleteData) return (
        <div>
            <Container className="p-5">
                <h1>Athletes</h1>
                <p><b>Failed to fetch data array. Press the button to try again...</b></p>
                <Button variant="primary" onClick={() => setTryAgain(true)}>Try Again</Button>
            </Container>
        </div>
    );
    
    return (
        <div>
            { loading ? (
                <h2>Loading...</h2>
            ) : (
                <div>
                    <Container className="py-5">
                        <h1>{athleteData.name}</h1>
                    </Container>
                    <Container className="mb-5">
                        <h2 className="mb-3">Photo</h2>
                        <img
                            src={athleteData.player_image}
                            style={{objectFit: 'cover', objectPosition: 'center', overflow: 'hidden', width: '100%', maxWidth: '300px'}}
                        />
                    </Container>
                    <Container className="mb-5">
                        <h2 className="mb-3">Stats</h2>
                        <p className="mb-0"><b>Age:</b> {athleteData.age}</p>
                        <p className="mb-0"><b>Height:</b> {athleteData.height}</p>
                        <p className="mb-0"><b>Weight:</b> {athleteData.weight}</p>
                        <p className="mb-0"><b>Birthplace:</b> {athleteData.birthplace}</p>
                        <p className="mb-0"><b>Nationality:</b> {athleteData.nationality}</p>
                        <p className="mb-0"><b>Fouls Committed:</b> {athleteData.fouls}</p>
                        <p className="mb-0"><b>Goals:</b> {athleteData.goals}</p>
                        <p className="mb-0"><b>Position:</b> {athleteData.position}</p>
                    </Container>
                    <Container className="mb-5">
                        <h2 className="mb-3">Connections</h2>
                        <Row className="justify-content-center">
                            <Col md={6} sm={12} className="px-5">
                                <h3>{athleteData.team}</h3>
                                <div>
                                    <img
                                        className="mb-3"
                                        src={athleteData.team_logo}
                                        style={{objectFit: 'cover', objectPosition: 'center', overflow: 'hidden', width: '100%', maxWidth: '200px'}}
                                    />
                                </div>
                                <Button className="mb-3" variant="primary" href={`/teams/${athleteData.team_id}`}>More Info</Button>
                            </Col>
                            <Col md={6} sm={12} className="px-5">
                                <h3>{athleteData.country}</h3>
                                <div>
                                    <img
                                        className="mb-3"
                                        src={athleteData.country_flag}
                                        style={{objectFit: 'cover', objectPosition: 'center', overflow: 'hidden', width: '100%', maxWidth: '400px'}}
                                    />
                                </div>
                                <Button className="mb-3" variant="primary" href={`/countries/${athleteData.country}`}>More Info</Button>
                            </Col>
                        </Row>
                    </Container>
                </div>
            )}
        </div>
    );
}
export default AthleteInstance;
