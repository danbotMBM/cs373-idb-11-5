import React, { useState, useEffect } from "react";
import axios from 'axios';
import { Row, Col, Container, Card, CardBody, CardImg, Button,
    Pagination, PaginationItem, PaginationLink, Input } from 'reactstrap';
import { Highlight } from "react-highlighter-ts";

function Athletes() {

    const athletesAPI = `https://api.soccerstars.me${window.location.pathname}`;

    const [athleteData, setAthleteData] = useState([]);
    const [loading, setLoading] = useState(false);
    const [tryAgain, setTryAgain] = useState(false);
    const [query, setQuery] = useState(null);
    const [currentPage, setCurrentPage] = useState(1);
    const [sortBy, setSortBy] = useState(null);
    const [desc, setDesc] = useState(false);
    const [filterBy, setFilterBy] = useState(null);
    const [filterOp, setFilterOp] = useState("=");
    const [filterAmt, setFilterAmt] = useState(null);

    useEffect(() => {
        document.title = `Soccer Stars | Athletes`;
    });

    useEffect(() => {
        setLoading(true);
        // let isSubscribed = true
        // console.log(isSortAscending)
        // if(isSortAscending) {
        //     setDesc(null)
        // } else {
        //     setDesc(true)
        // }
        axios.get(athletesAPI, {
            params: {
                page: currentPage,
                search: query,
                sort: sortBy,
                desc: desc,
                filter: "" + filterBy + filterOp + filterAmt
            }
        })
            .then((response) => {
                setAthleteData(response.data);
            })
            .catch(function (error) {
                console.error(error);
            })
            .then(function () {
                setLoading(false);
                setTryAgain(false);
            });
        // return () => isSubscribed = false
    }, [athletesAPI, tryAgain, query, currentPage, sortBy, desc, filterBy, filterOp, filterAmt]);

    const handleQueryChange = (e) => {
        if (e.target.value === '') {
            setQuery(null)
        } else {
            setQuery(e.target.value);
        }
    };

    const handleSortChange = (e) => {
        if (e.target.value === '') {
            setSortBy(null)
        } else {
            setSortBy(e.target.value)
        }
    };

    const handleDescChange = (e) => {
        if (e.target.value == 0) {
            setDesc(false)
        } else {
            setDesc(e.target.value)
        }
    };

    const handleFilterChange = (e) => {
        if (e.target.value === '') {
            setFilterBy(null)
        } else {
            setFilterBy(e.target.value)
        }
    };

    const handleFilterOpChange = (e) => {
        setFilterOp(e.target.value)
    };

    const handleFilterAmtChange = (e) => {
        if (e.target.value === '') {
            setFilterAmt(null)
        } else {
            setFilterAmt(e.target.value)
        }
    };

    if (!athleteData || !(athleteData.data)) return (
        <div>
            <Container className="p-5">
                <h1>Athletes</h1>
                <p>0 total entries</p>
                <p><b>Failed to fetch data array. Press the button to try again...</b></p>
                <Button variant="primary" onClick={() => setTryAgain(true)}>Try Again</Button>
            </Container>
        </div>
    );

    // const paginateFunc = (pageNumber) => setCurrentPage(pageNumber);
    
    return (
        <div>
            <Container className="py-5">
                <h1>Athletes</h1>
                <p className="mb-0">{athleteData.count} total entries</p>
                <p>{athleteData.paging.total} total pages</p>
                {/* <p className="mb-0"><u><b>Athlete Data from API</b> ({athletesAPI})</u></p>
                <p>{JSON.stringify(athleteData.data)}</p> */}
                <Input
                    type="search"
                    placeholder="Search"
                    onChange={handleQueryChange}
                    value={query}
                />
            </Container>
            <Container className="mb-5">
                <p><b>Sort</b></p>
                <Row className="mb-3">
                    <Col>
                        <Input
                            type="select"
                            onChange={handleSortChange}
                            value={sortBy}
                        >
                            <option value="">None</option>
                            <option value="name">Name</option>
                            <option value="age">Age</option>
                            <option value="height">Height</option>
                            <option value="weight">Weight</option>
                            <option value="goals">Goals</option>
                            <option value="position">Position</option>
                            <option value="nationality">Nationality</option>
                        </Input>
                    </Col>
                    <Col>
                        <Input
                            type="select"
                            onChange={handleDescChange}
                            value={desc}
                        >
                            <option value={0}>Ascending</option>
                            <option value={true}>Descending</option>
                        </Input>
                    </Col>
                </Row>
                <p><b>Filter</b></p>
                <Row className="mb-3">
                    <Col>
                        <Input
                            type="select"
                            onChange={handleFilterChange}
                            value={filterBy}
                        >
                            <option value="">None</option>
                            <option value="age">Age</option>
                            <option value="height">Height</option>
                            <option value="weight">Weight</option>
                            <option value="goals">Goals</option>
                        </Input>
                    </Col>
                    <Col>
                        <Input
                            type="select"
                            onChange={handleFilterOpChange}
                            value={filterOp}
                        >
                            <option value="=">Equal To</option>
                            <option value=">">Greater Than</option>
                            <option value="<">Less Than</option>
                        </Input>
                    </Col>
                    <Col>
                        <Input
                            type="number"
                            placeholder="Value"
                            onChange={handleFilterAmtChange}
                            value={filterAmt}
                        />
                    </Col>
                </Row>
            </Container>
            <Container className="pb-2">
                <p>{sortBy == null ? "" : `Currently sorted by ${sortBy}${desc ? " descending" : ""}`}</p>
            </Container>
            <Container>
                { loading ? (
                    <h2>Loading...</h2>
                ) : (
                    <Row xs={2} sm={3} md={4} lg={6} className="mb-5 justify-content-center">
                        {(athleteData.data).map((athlete) => (
                            <Card className="px-0">
                                <CardImg 
                                    src={athlete.player_image}
                                    style={{objectFit: 'cover', objectPosition: 'center', overflow: 'hidden', minHeight: '250px', height: '15vw'}}
                                />
                                <CardBody className="d-flex flex-column">
                                    <h4 className="mb-auto"><Highlight search={query} matchStyle={{backgroundColor: 'orange'}}>{athlete.name}</Highlight></h4>
                                    <p className="mb-0"><u><b>Age</b></u></p>
                                    <p className="mb-0">{athlete.age}</p>
                                    <p className="mb-0"><u><b>Height</b></u></p>
                                    <p className="mb-0">{athlete.height}</p>
                                    <p className="mb-0"><u><b>Weight</b></u></p>
                                    <p className="mb-0">{athlete.weight}</p>
                                    <p className="mb-0"><u><b>Nationality</b></u></p>
                                    <p className="mb-0"><Highlight search={query} matchStyle={{backgroundColor: 'orange'}}>{athlete.nationality}</Highlight></p>
                                    <p className="mb-0"><u><b>Position</b></u></p>
                                    <p className="mb-0">{athlete.position}</p>
                                    <p className="mb-0"><u><b>Goals</b></u></p>
                                    <p>{athlete.goals}</p>
                                    {/* <p><b>ID:</b> {athlete.id}</p> */}
                                    <Button variant="primary" href={`/athletes/${athlete.id}`}>More Info</Button>
                                </CardBody>
                            </Card>
                        ))}
                    </Row>
                )}
            </Container>
            <Pagination className="mb-5 d-flex justify-content-center">
                <PaginationItem disabled={athleteData.paging.current == 1}>
                    <PaginationLink
                        first
                        onClick={() => setCurrentPage(1)}
                    />
                </PaginationItem>
                <PaginationItem disabled={athleteData.paging.current == 1}>
                    <PaginationLink
                        previous
                        onClick={() => setCurrentPage(athleteData.paging.current - 1)}
                    />
                </PaginationItem>
                <PaginationItem>
                    <PaginationLink href="#">
                        {athleteData.paging.current}
                    </PaginationLink>
                </PaginationItem>
                <PaginationItem disabled={athleteData.paging.current == athleteData.paging.total}>
                    <PaginationLink
                        next
                        onClick={() => setCurrentPage(athleteData.paging.current + 1)}
                    />
                </PaginationItem>
                <PaginationItem disabled={athleteData.paging.current == athleteData.paging.total}>
                    <PaginationLink
                        last
                        onClick={() => setCurrentPage(athleteData.paging.total)}
                    />
                </PaginationItem>
            </Pagination>
        </div>
    );
}
export default Athletes;
