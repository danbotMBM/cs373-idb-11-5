import React, { useState, useEffect } from "react";
import axios from 'axios';
import { Row, Col, Container, Card, CardBody, CardImg, Button,
    Pagination, PaginationItem, PaginationLink, Input } from 'reactstrap';
import { Highlight } from "react-highlighter-ts";

// Code derived from https://gitlab.com/catchingcongress/catchingcongress/-/blob/main/frontend/src/pages/committees/CommitteeInstance.tsx
function numberWithCommas(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',');
}


function Countries() {

    const countriesAPI = `https://api.soccerstars.me${window.location.pathname}`;

    const [countryData, setCountryData] = useState([]);
    const [loading, setLoading] = useState(false);
    const [tryAgain, setTryAgain] = useState(false);
    const [query, setQuery] = useState(null);
    const [currentPage, setCurrentPage] = useState(1);
    const [sortBy, setSortBy] = useState(null);
    const [desc, setDesc] = useState(false);
    const [filterBy, setFilterBy] = useState(null);
    const [filterOp, setFilterOp] = useState("=");
    const [filterAmt, setFilterAmt] = useState(null);

    useEffect(() => {
        document.title = `Soccer Stars | Countries`;
    });

    useEffect(() => {
        setLoading(true);
        // let isSubscribed = true
        // console.log(isSortAscending)
        // if(isSortAscending) {
        //     setDesc(null)
        // } else {
        //     setDesc(true)
        // }
        axios.get(countriesAPI, {
            params: {
                page: currentPage,
                search: query,
                sort: sortBy,
                desc: desc,
                filter: "" + filterBy + filterOp + filterAmt
            }
        })
            .then((response) => {
                setCountryData(response.data);
            })
            .catch(function (error) {
                console.error(error);
            })
            .then(function () {
                setLoading(false);
                setTryAgain(false);
            });
        // return () => isSubscribed = false
    }, [countriesAPI, tryAgain, query, currentPage, sortBy, desc, filterBy, filterOp, filterAmt]);

    const handleQueryChange = (e) => {
        if (e.target.value === '') {
            setQuery(null)
        } else {
            setQuery(e.target.value)
        }
    };

    const handleSortChange = (e) => {
        if (e.target.value === '') {
            setSortBy(null)
        } else {
            setSortBy(e.target.value)
        }
    };

    const handleDescChange = (e) => {
        if (e.target.value == 0) {
            setDesc(false)
        } else {
            setDesc(e.target.value)
        }
    };

    const handleFilterChange = (e) => {
        if (e.target.value === '') {
            setFilterBy(null)
        } else {
            setFilterBy(e.target.value)
        }
    };

    const handleFilterOpChange = (e) => {
        setFilterOp(e.target.value)
    };

    const handleFilterAmtChange = (e) => {
        if (e.target.value === '') {
            setFilterAmt(null)
        } else {
            setFilterAmt(e.target.value)
        }
    };

    if (!countryData || !(countryData.data)) return (
        <div>
            <Container className="p-5">
                <h1>Countries</h1>
                <p>0 total entries</p>
                <p><b>Failed to fetch data array. Press the button to try again...</b></p>
                <Button variant="primary" onClick={() => setTryAgain(true)}>Try Again</Button>
            </Container>
        </div>
    );
    
    return (
        <div>
            <Container className="py-5">
                <h1>Countries</h1>
                <p className="mb-0">{countryData.count} total entries</p>
                <p>{countryData.paging.total} total pages</p>
                {/* <p className="mb-0"><u><b>Country Data from API</b> ({countriesAPI})</u></p>
                <p>{JSON.stringify(countryData.data)}</p> */}
                <Input
                    type="search"
                    placeholder="Search"
                    onChange={handleQueryChange}
                    value={query}
                />
            </Container>
            <Container className="mb-5">
                <p><b>Sort</b></p>
                <Row className="mb-3">
                    <Col>
                        <Input
                            type="select"
                            onChange={handleSortChange}
                            value={sortBy}
                        >
                            <option value="">None</option>
                            <option value="name">Name</option>
                            <option value="population">Population</option>
                            <option value="area">Area</option>
                            <option value="latitude">Latitude</option>
                            <option value="longitude">Longitude</option>
                            <option value="region">Region</option>
                        </Input>
                    </Col>
                    <Col>
                        <Input
                            type="select"
                            onChange={handleDescChange}
                            value={desc}
                        >
                            <option value={0}>Ascending</option>
                            <option value={true}>Descending</option>
                        </Input>
                    </Col>
                </Row>
                <p><b>Filter</b></p>
                <Row className="mb-3">
                    <Col>
                        <Input
                            type="select"
                            onChange={handleFilterChange}
                            value={filterBy}
                        >
                            <option value="">None</option>
                            <option value="population">Population</option>
                            <option value="area">Area</option>
                            <option value="latitude">Latitude</option>
                            <option value="longitude">Longitude</option>
                            <option value="region">Region</option>
                        </Input>
                    </Col>
                    <Col>
                        <Input
                            type="select"
                            onChange={handleFilterOpChange}
                            value={filterOp}
                        >
                            <option value="=">Equal To</option>
                            <option value=">">Greater Than</option>
                            <option value="<">Less Than</option>
                        </Input>
                    </Col>
                    <Col>
                        <Input
                            type="number"
                            placeholder="Value"
                            onChange={handleFilterAmtChange}
                            value={filterAmt}
                        />
                    </Col>
                </Row>
            </Container>
            <Container className="pb-2">
                <p>{sortBy == null ? "" : `Currently sorted by ${sortBy}${desc ? " descending" : ""}`}</p>
            </Container>
            <Container>
                { loading ? (
                    <h2>Loading...</h2>
                ) : (
                    <Row xs={2} sm={3} md={4} lg={6} className="mb-5 justify-content-center">
                        {(countryData.data).map((country) => (
                            <Card className="px-0">
                                <CardImg 
                                    src={country.flag_url}
                                    style={{objectFit: 'cover', objectPosition: 'center', overflow: 'hidden', minHeight: '100px', maxHeight: '150px', height: '15vw'}}
                                />
                                <CardBody className="d-flex flex-column">
                                    <h4 className="mb-auto"><Highlight search={query} matchStyle={{backgroundColor: 'orange'}}>{country.name}</Highlight></h4>
                                    <p className="mb-0"><u><b>Population</b></u></p>
                                    <p className="mb-0">{numberWithCommas(country.population)}</p>
                                    <p className="mb-0"><u><b>Longitude</b></u></p>
                                    <p className="mb-0">{country.longitude}</p>
                                    <p className="mb-0"><u><b>Latitude</b></u></p>
                                    <p className="mb-0">{country.latitude}</p>
                                    <p className="mb-0"><u><b>Area</b></u></p>
                                    <p className="mb-0">{numberWithCommas(country.area)} km&sup2;</p>
                                    <p className="mb-0"><u><b>Region</b></u></p>
                                    <p><Highlight search={query} matchStyle={{backgroundColor: 'orange'}}>{country.region}</Highlight></p>
                                    {/* <p><b>ID:</b> {country.id}</p> */}
                                    <Button variant="primary" href={`/countries/${country.name}`}>More Info</Button>
                                </CardBody>
                            </Card>
                        ))}
                    </Row>
                )}
            </Container>
            <Pagination className="mb-5 d-flex justify-content-center">
                <PaginationItem disabled={countryData.paging.current == 1}>
                    <PaginationLink
                        first
                        onClick={() => setCurrentPage(1)}
                    />
                </PaginationItem>
                <PaginationItem disabled={countryData.paging.current == 1}>
                    <PaginationLink
                        previous
                        onClick={() => setCurrentPage(countryData.paging.current - 1)}
                    />
                </PaginationItem>
                <PaginationItem>
                    <PaginationLink href="#">
                        {countryData.paging.current}
                    </PaginationLink>
                </PaginationItem>
                <PaginationItem disabled={countryData.paging.current == countryData.paging.total}>
                    <PaginationLink
                        next
                        onClick={() => setCurrentPage(countryData.paging.current + 1)}
                    />
                </PaginationItem>
                <PaginationItem disabled={countryData.paging.current == countryData.paging.total}>
                    <PaginationLink
                        last
                        onClick={() => setCurrentPage(countryData.paging.total)}
                    />
                </PaginationItem>
            </Pagination>
        </div>
    );
}
export default Countries;
