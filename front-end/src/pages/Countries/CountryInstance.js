import React, { useState, useEffect } from "react";
import axios from 'axios';
import { Row, Col, Container, Button, Alert } from 'reactstrap';

// Code derived from https://gitlab.com/catchingcongress/catchingcongress/-/blob/main/frontend/src/pages/committees/CommitteeInstance.tsx
function numberWithCommas(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',');
}

function CountryInstance() {

    const countriesAPI = `https://api.soccerstars.me${window.location.pathname}`;

    const [countryData, setCountryData] = useState([]);
    const [loading, setLoading] = useState(false);
    const [tryAgain, setTryAgain] = useState(false);

    useEffect(() => {
        setLoading(true);
        axios.get(countriesAPI)
            .then((response) => {
                setCountryData(response.data);
            })
            .catch(function (error) {
                console.error(error);
            })
            .then(function () {
                setLoading(false);
                setTryAgain(false);
            });
    }, [countriesAPI, tryAgain]);

    useEffect(() => {
        document.title = `Soccer Stars | Countries | ${countryData.name}`;
    });

    if (!countryData) return (
        <div>
            <Container className="p-5">
                <h1>Countries</h1>
                <p><b>Failed to fetch data array. Press the button to try again...</b></p>
                <Button variant="primary" onClick={() => setTryAgain(true)}>Try Again</Button>
            </Container>
        </div>
    );
    
    return (
        <div>
            { loading ? (
                <h2>Loading...</h2>
            ) : (
                <div>
                    <Container className="py-5">
                        <h1>{countryData.name}</h1>
                    </Container>
                    <Container className="mb-5">
                        <h2 className="mb-3">Flag</h2>
                        <img
                            src={countryData.flag_url}
                            style={{objectFit: 'cover', objectPosition: 'center', overflow: 'hidden', width: '100%', maxWidth: '500px'}}
                        />
                    </Container>
                    <Container className="mb-5">
                        <h2 className="mb-3">Stats</h2>
                        <p className="mb-0"><b>Population:</b> {countryData.population != null ? numberWithCommas(countryData.population) : ""}</p>
                        <p className="mb-0"><b>Latitude:</b> {countryData.latitude}</p>
                        <p className="mb-0"><b>Longitude:</b> {countryData.longitude}</p>
                        <p className="mb-0"><b>Area:</b> {countryData.area != null ? numberWithCommas(countryData.area) : ""} km&sup2;</p>
                        {/* <p className="mb-0"><b>Native Names:</b> <i>Needs to be updated</i></p> */}
                        <p className="mb-0"><b>Capital:</b> {countryData.capital}</p>
                        <p className="mb-0"><b>Continent:</b> {countryData.continent}</p>
                        <p className="mb-0"><b>Region:</b> {countryData.region}</p>
                        <p className="mb-0"><b>League:</b> {countryData.league}</p>
                        {/* <p className="mb-0"><b>Currencies:</b> <i>Needs to be updated</i></p> */}
                        {/* <p className="mb-0"><b>Languages:</b> <i>Needs to be updated</i></p> */}
                    </Container>
                    <Container className="mb-5">
                        <h2 className="mb-3">Map</h2>
                        <iframe
                            width="100%"
                            height="500"
                            frameBorder="0"
                            referrerPolicy="no-referrer-when-downgrade"
                            src={"https://www.google.com/maps/embed/v1/place?key=AIzaSyCFEwlBU9cPr1yJdXKV1Z_LZ6s2wwfL1ko&q=" + countryData.name}
                            allowFullScreen
                        />
                    </Container>
                    <Container className="mb-5">
                        <h2 className="mb-3">Trending News</h2>
                            { countryData.news_element == null ? (
                                <Alert color="danger" className="mb-0">Failed to fetch news article!</Alert>
                            ) : (
                                <div style={{border: '1px solid black'}}>
                                    <iframe
                                        width="100%"
                                        height="500"
                                        frameBorder="0"
                                        referrerPolicy="no-referrer-when-downgrade"
                                        src={countryData.news_element}
                                        allowFullScreen
                                    />
                                </div>
                            )}
                    </Container>
                    <Container className="mb-5">
                        <h2 className="mb-3">Connections</h2>
                        <Row className="justify-content-center">
                            <Col md={6} sm={12} className="px-5">
                                <h3>{countryData.teams_from_country != null ? countryData.teams_from_country.name : ""}</h3>
                                <div>
                                    <img
                                        className="mb-3"
                                        src={countryData.teams_from_country != null ? countryData.teams_from_country.team_logo : ""}
                                        style={{objectFit: 'cover', objectPosition: 'center', overflow: 'hidden', width: '100%', maxWidth: '200px'}}
                                    />
                                </div>
                                <Button className="mb-3" variant="primary" href={countryData.teams_from_country != null ? `/teams/${countryData.teams_from_country.id}` : ""}>More Info</Button>
                            </Col>
                            <Col md={6} sm={12} className="px-5">
                                <h3>{countryData.athletes_from_country != null ? countryData.athletes_from_country.name : ""}</h3>
                                <div>
                                    <img
                                        className="mb-3"
                                        src={countryData.athletes_from_country != null ? countryData.athletes_from_country.player_image : ""}
                                        style={{objectFit: 'cover', objectPosition: 'center', overflow: 'hidden', width: '100%', maxWidth: '200px'}}
                                    />
                                </div>
                                <Button className="mb-3" variant="primary" href={countryData.athletes_from_country != null ? `/athletes/${countryData.athletes_from_country.id}` : ""}>More Info</Button>
                            </Col>
                        </Row>
                    </Container>
                </div>
            )}
        </div>
    );
}
export default CountryInstance;
