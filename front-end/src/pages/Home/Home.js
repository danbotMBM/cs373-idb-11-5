import React from 'react';
import carousel0 from "../../assets/carousel/0.jpg";
import carousel1 from "../../assets/carousel/1.jpg";
import carousel2 from "../../assets/carousel/2.jpg";
import carousel3 from "../../assets/carousel/3.jpg";
import carousel4 from "../../assets/carousel/4.jpg";
import countries from "../../assets/countries.jpg";
import teams from "../../assets/teams.jpg";
import athletes from "../../assets/athletes.jpg";
import { Carousel, Container, Card, Row, Col, Button } from "react-bootstrap";

function Home() {

    const linkTo = (route) => {
        window.location.href = route;
    };

    return (
        <>
        <Carousel id='imageCarousel'>
            <Carousel.Item>
                <img 
                    className="d-block w-100"
                    src={carousel0}
                    alt="Trophy"
                    style={{objectFit: 'cover', objectPosition: 'center', overflow: 'hidden', height: '50vw', maxHeight:'80vh'}}
                />
            </Carousel.Item>
            <Carousel.Item>
                <img 
                    className="d-block w-100"
                    src={carousel1}
                    alt="Goal"
                    style={{objectFit: 'cover', objectPosition: 'center', overflow: 'hidden', height: '50vw', maxHeight:'80vh'}}
                />
            </Carousel.Item>
            <Carousel.Item>
                <img 
                    className="d-block w-100"
                    src={carousel2}
                    alt="UT Players"
                    style={{objectFit: 'cover', objectPosition: 'center', overflow: 'hidden', height: '50vw', maxHeight:'80vh'}}
                />
            </Carousel.Item>
            <Carousel.Item>
                <img 
                    className="d-block w-100"
                    src={carousel3}
                    alt="Stadium"
                    style={{objectFit: 'cover', objectPosition: 'center', overflow: 'hidden', height: '50vw', maxHeight:'80vh'}}
                />
            </Carousel.Item>
            <Carousel.Item>
                <img 
                    className="d-block w-100"
                    src={carousel4}
                    alt="Players"
                    style={{objectFit: 'cover', objectPosition: 'center', overflow: 'hidden', height: '50vw', maxHeight:'80vh'}}
                />
            </Carousel.Item>
        </Carousel>

        <Container className="my-5">
            <h1>Soccer Stars</h1>
            <p style={{ fontSize: '20px'}}>Soccer Stars aims to promote engagement with soccer on a global scale, relate teams to the events and successes of their respective countries, and expand recognition of global cultures.</p>
            <Button id='AboutUsButton' variant="outline-primary" className="mt-auto" href="about">Learn More About Us</Button>
        </Container>

        <Row className="justify-content-center">
            <Col lg={4} md={10} sm={10} xs={10} className="card px-0 mb-3">
                <Card.Img
                    variant="top"
                    className="w-100"
                    src={countries}
                    alt="Countries"
                    style={{objectFit: 'cover', objectPosition: 'center', overflow: 'hidden', height: '15vw', minHeight: '200px'}}
                />
                <Card.Body className="d-flex flex-column">
                    <Card.Title>
                        <h4>Countries</h4>
                    </Card.Title>
                    <Card.Text>
                        <p>A collection of countries that have representative soccer teams.</p>
                    </Card.Text>
                    <Button id='ViewCountriesButton' variant="primary" className="mt-auto" href="countries">View Countries</Button>
                </Card.Body>
            </Col>
            <Col lg={4} md={10} sm={10} xs={10} className="card px-0 mb-3">
                <Card.Img
                    variant="top"
                    className="w-100"
                    src={teams}
                    alt="Teams"
                    style={{objectFit: 'cover', objectPosition: 'center', overflow: 'hidden', height: '15vw', minHeight: '200px'}}
                />
                <Card.Body className="d-flex flex-column">
                    <Card.Title>
                        <h4>Teams</h4>
                    </Card.Title>
                    <Card.Text>
                        <p>A collection of globally recognized soccer teams participating in an official league.</p>
                    </Card.Text>
                    <Button id='ViewTeamsButton' variant="primary" className="mt-auto" href="teams">View Teams</Button>
                </Card.Body>
            </Col>
            <Col lg={4} md={10} sm={10} xs={10} className="card px-0 mb-3">
                <Card.Img
                    variant="top"
                    className="w-100"
                    src={athletes}
                    alt="Athletes"
                    style={{objectFit: 'cover', objectPosition: 'center', overflow: 'hidden', height: '15vw', minHeight: '200px'}}
                />
                <Card.Body className="d-flex flex-column">
                    <Card.Title>
                        <h4>Athletes</h4>
                    </Card.Title>
                    <Card.Text>
                        <p>A collection of soccer athletes currently on an official team roster.</p>
                    </Card.Text>
                    <Button id='ViewAthletesButton' variant="primary" className="mt-auto" href="athletes">View Athletes</Button>
                </Card.Body>
            </Col>
        </Row>
    </>
    );
}
export default Home;
