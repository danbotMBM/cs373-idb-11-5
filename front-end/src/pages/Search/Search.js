import React, { useState, useEffect } from "react";
import axios from 'axios';
import { Row, Container, Card, CardBody, CardImg, Button, Input } from 'reactstrap';
import { Highlight } from "react-highlighter-ts";

// Code derived from https://gitlab.com/catchingcongress/catchingcongress/-/blob/main/frontend/src/pages/committees/CommitteeInstance.tsx
function numberWithCommas(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',');
}

function Search() {

    const API = `https://api.soccerstars.me${window.location.pathname}`;

    const [countryData, setCountryData] = useState([]);
    const [teamData, setTeamData] = useState([]);
    const [athleteData, setAthleteData] = useState([]);
    const [query, setQuery] = useState(null);

    useEffect(() => {
        document.title = `Soccer Stars | Search`;
    });

    useEffect(() => {
        if (query != null) {
            axios.get(API, {
                params: {
                    search: query
                }
            })
                .then((response) => {
                    setCountryData(response.data.Country.data);
                    setTeamData(response.data.Teams.data);
                    setAthleteData(response.data.Athletes.data);
                })
                .catch(function (error) {
                    console.error(error);
                });
        } else {
            setCountryData([]);
            setTeamData([]);
            setAthleteData([]);
        }
    }, [query]);

    const handleQueryChange = (e) => {
        if (e.target.value === '') {
            setQuery(null)
        } else {
            setQuery(e.target.value);
        }
    };
    
    return (
        <div>
            <Container className="py-5">
                <h1 className="pb-3">Search</h1>
                <Input
                    type="search"
                    placeholder="Search"
                    onChange={handleQueryChange}
                    value={query}
                />
            </Container>
            { countryData.length == 0 ? <p className="mb-0"/> : (
            <Container>
                <h2 className="mb-3">Countries</h2>
                <Row xs={2} sm={3} md={4} lg={6} className="mb-5 justify-content-center">
                    {(countryData).map((country) => (
                        <Card className="px-0">
                            <CardImg 
                                src={country.flag_url}
                                style={{objectFit: 'cover', objectPosition: 'center', overflow: 'hidden', minHeight: '100px', maxHeight: '150px', height: '15vw'}}
                            />
                            <CardBody className="d-flex flex-column">
                                <h4 className="mb-auto"><Highlight search={query} matchStyle={{backgroundColor: 'orange'}}>{country.name}</Highlight></h4>
                                <p className="mb-0"><u><b>Population</b></u></p>
                                <p className="mb-0">{numberWithCommas(country.population)}</p>
                                <p className="mb-0"><u><b>Longitude</b></u></p>
                                <p className="mb-0">{country.longitude}</p>
                                <p className="mb-0"><u><b>Latitude</b></u></p>
                                <p className="mb-0">{country.latitude}</p>
                                <p className="mb-0"><u><b>Area</b></u></p>
                                <p className="mb-0">{numberWithCommas(country.area)} km&sup2;</p>
                                <p className="mb-0"><u><b>Region</b></u></p>
                                <p><Highlight search={query} matchStyle={{backgroundColor: 'orange'}}>{country.region}</Highlight></p>
                                <Button variant="primary" href={`/countries/${country.name}`}>More Info</Button>
                            </CardBody>
                        </Card>
                    ))}
                </Row>
            </Container>
            )}
            { teamData.length == 0 ? <p className="mb-0"/> : (
            <Container>
                <h2 className="mb-3">Teams</h2>
                <Row xs={2} sm={3} md={4} lg={6} className="mb-5 justify-content-center">
                    {(teamData).map((team) => (
                        <Card className="px-0">
                            <CardImg 
                                src={team.team_logo}
                                className="m-auto"
                                style={{objectFit: 'cover', objectPosition: 'center', overflow: 'hidden', maxHeight: '200px', maxWidth: '200px'}}
                            />
                            <CardBody className="d-flex flex-column">
                                <h4 className="mb-auto"><Highlight search={query} matchStyle={{backgroundColor: 'orange'}}>{team.name}</Highlight></h4>
                                <p className="mb-0"><u><b>Country</b></u></p>
                                <p className="mb-0"><Highlight search={query} matchStyle={{backgroundColor: 'orange'}}>{team.governing_country}</Highlight></p>
                                <p className="mb-0"><u><b>Games</b></u></p>
                                <p className="mb-0">{team.total_games}</p>
                                <p className="mb-0"><u><b>Wins</b></u></p>
                                <p className="mb-0">{team.wins}</p>
                                <p className="mb-0"><u><b>Losses</b></u></p>
                                <p className="mb-0">{team.losses}</p>
                                <p className="mb-0"><u><b>Win Percentage</b></u></p>
                                <p className="mb-0">{team.total_games == 0 ? "0" : Math.round(100 * (team.wins / team.total_games))}%</p>
                                <p className="mb-0"><u><b>Goals</b></u></p>
                                <p className="mb-0">{team.goals}</p>
                                <p className="mb-0"><u><b>Average Goals/Game</b></u></p>
                                <p className="mb-0">{team.goals_avg}</p>
                                <p className="mb-0"><u><b>Goals Against</b></u></p>
                                <p>{team.goals_against}</p>
                                <Button variant="primary" href={`/teams/${team.id}`}>More Info</Button>
                            </CardBody>
                        </Card>
                    ))}
                </Row>
            </Container>
            )}
            { athleteData.length == 0 ? <p className="mb-0"/> : (
            <Container>
                <h2 className="mb-3">Athletes</h2>
                <Row xs={2} sm={3} md={4} lg={6} className="mb-5 justify-content-center">
                    {(athleteData).map((athlete) => (
                        <Card className="px-0">
                            <CardImg 
                                src={athlete.player_image}
                                style={{objectFit: 'cover', objectPosition: 'center', overflow: 'hidden', minHeight: '250px', height: '15vw'}}
                            />
                            <CardBody className="d-flex flex-column">
                                <h4 className="mb-auto"><Highlight search={query} matchStyle={{backgroundColor: 'orange'}}>{athlete.name}</Highlight></h4>
                                <p className="mb-0"><u><b>Age</b></u></p>
                                <p className="mb-0">{athlete.age}</p>
                                <p className="mb-0"><u><b>Height</b></u></p>
                                <p className="mb-0">{athlete.height}</p>
                                <p className="mb-0"><u><b>Weight</b></u></p>
                                <p className="mb-0">{athlete.weight}</p>
                                <p className="mb-0"><u><b>Nationality</b></u></p>
                                <p className="mb-0"><Highlight search={query} matchStyle={{backgroundColor: 'orange'}}>{athlete.nationality}</Highlight></p>
                                <p className="mb-0"><u><b>Position</b></u></p>
                                <p className="mb-0">{athlete.position}</p>
                                <p className="mb-0"><u><b>Goals</b></u></p>
                                <p>{athlete.goals}</p>
                                <Button variant="primary" href={`/athletes/${athlete.id}`}>More Info</Button>
                            </CardBody>
                        </Card>
                    ))}
                </Row>
            </Container>
            )}
        </div>
    );
}
export default Search;
