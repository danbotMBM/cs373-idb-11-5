import React, { useState, useEffect } from "react";
import axios from "axios";
import { Row, Container, Card, CardBody, CardImg, Button, Input, Alert } from 'reactstrap';
import { PieChart, Pie, Legend, ScatterChart, Scatter, BarChart, Bar, XAxis, YAxis, CartesianGrid, Tooltip, ResponsiveContainer } from 'recharts';

// code modified from group @ https://gitlab.com/maxwthomas/cs373-idb/-/blob/phase4/frontend/src/pages/Visualizations/Visualizations.js
function ProviderVisualizations() {

    const airportsAPI = `https://api.parkdex.me/airports`;
    const hotelsAPI = `https://api.parkdex.me/hotels`;
    const parksAPI = `https://api.parkdex.me/parks`;

    const [loading, setLoading] = useState(false);
    const [loading2, setLoading2] = useState(false);

    const [vis1Data, setVis1Data] = useState([]);
    const [vis2Data, setVis2Data] = useState([]);
    const [vis3Data, setVis3Data] = useState([]);

    const states = ["AK", "AL", "AR", "AZ", "CA", "CO", "CT", "DC", "DE", "FL",
                    "GA", "GU", "HI", "IA", "ID", "IL", "IN", "KS", "KY", "LA",
                    "MA", "MD", "ME", "MI", "MN", "MO", "MP", "MS", "MT", "NC",
                    "ND", "NE", "NH", "NJ", "NM", "NV", "NY", "OH", "OK", "OR",
                    "PA", "PR", "RI", "SC", "SD", "TN", "TX", "UT", "VA", "VT",
                    "WA", "WI", "WV", "WY"];      

    useEffect(() => {
        document.title = `Soccer Stars | Provider Visualizations`;
    });

    useEffect(() => {
        setLoading(true);
        setLoading2(true);

        let data = [];

        axios.get(`${airportsAPI}?type=small_airport`)
            .then((response) => {
                data.push({name: "Small", value: response.data.count});
            })
            .catch(function (error) {
                console.error(error);
            })
            .then(function () {
                axios.get(`${airportsAPI}?type=medium_airport`)
                    .then((response) => {
                        data.push({name: "Medium", value: response.data.count});
                    })
                    .catch(function (error) {
                        console.error(error);
                    })
                    .then(function () {
                        axios.get(`${airportsAPI}?type=large_airport`)
                            .then((response) => {
                                data.push({name: "Large", value: response.data.count});
                                setVis1Data(data);
                                data = [];
                                for (let i = 1; i <= response.data.total_pages; i++) {
                                    axios.get(`${airportsAPI}?type=large_airport&page=${i}`)
                                        .then((response) => {
                                            setLoading(true);
                                            for (const airport of response.data.data) {
                                                if (airport.elevation && airport.nearest_hotels[0].dist) {
                                                    data.push({elevation: airport.elevation, hotel: airport.nearest_hotels[0].dist});
                                                }
                                            }
                                        })
                                        .catch(function (error) {
                                            console.error(error);
                                        })
                                        .then(function () {
                                            setVis2Data(data);
                                            setLoading(false);
                                        });
                                }
                            })
                            .catch(function (error) {
                                console.error(error);
                            })
                            .then(function () {
                                
                            });
                    });
            });
            let data2 = [];
            for (const state of states) {
                axios.get(`${parksAPI}?state=${state}`)
                    .then((response) => {
                        data2.push({name: state, count: response.data.count});
                    })
                    .catch(function (error) {
                        console.error(error);
                    })
                    .then(function () {
                        setVis3Data(data2);
                        if (state == states[states.length - 1]) {
                            setLoading2(false);
                        }
                    });
            }
    }, [airportsAPI, parksAPI]);

    return (
        <div>
            <Container className="py-5">
                <h1 className="pb-3">Provider Visualizations</h1>
            </Container>
            <Container className="mb-3">
                <h2 className="mb-3">Number of Airports by Size</h2>
                <p>What is the distribution of airports by size across the US?</p>
                    { loading ? (
                        <Alert color="warning" className="mb-0">Loading data...</Alert>
                    ) : (
                        <ResponsiveContainer width="100%" height={600} key={`rc_${vis1Data.length}`}>
                            <BarChart
                                key={`bc_${vis1Data.length}`}
                                data={vis1Data}
                                margin={{
                                    top: 50,
                                    right: 50,
                                    bottom: 50,
                                    left: 50
                                }}
                            >
                                <CartesianGrid strokeDasharray="3 3" />
                                <XAxis key={`x_${vis1Data.length}`} dataKey="name" name="Airport Size" />
                                <YAxis key={`y_${vis1Data.length}`} name="Quantity" />
                                <Tooltip />
                                <Bar dataKey="value" name="Quantity" fill="#8884d8" />
                            </BarChart>
                        </ResponsiveContainer>
                    )}
            </Container>
            <Container className="mb-3">
                <h2 className="mb-3">Airport Elevation vs Nearest Hotel for Large Airports</h2>
                <p>Does the distance to the nearest hotel change alongside the elevation of an airport?</p>
                    { loading ? (
                        <Alert color="warning" className="mb-0">Loading data...</Alert>
                    ) : (
                        <ResponsiveContainer width="100%" height={600} key={`rc_${vis2Data.length}`}>
                            <ScatterChart
                                key={`sc_${vis2Data.length}`}
                                margin={{
                                    top: 50,
                                    right: 50,
                                    bottom: 50,
                                    left: 50
                                }}
                            >
                                <CartesianGrid />
                                <XAxis key={`x_${vis2Data.length}`} type="number" dataKey="elevation" name="Elevation" unit=" ft" />
                                <YAxis key={`y_${vis2Data.length}`} type="number" dataKey="hotel" name="Nearest Hotel" unit=" miles" />
                                <Tooltip cursor={{ strokeDasharray: '3 3' }} />
                                <Scatter data={vis2Data} fill="#8884d8" />
                            </ScatterChart>
                        </ResponsiveContainer>
                    )}
            </Container>
            <Container className="mb-3">
                <h2 className="mb-3">Number of Parks by State</h2>
                <p>Which states offer the greatest number of parks?</p>
                    { loading2 ? (
                        <Alert color="warning" className="mb-0">Loading data...</Alert>
                    ) : (
                        <ResponsiveContainer width="100%" height={600} key={`rc_${vis3Data.length}`}>
                            <PieChart key={`pc_${vis3Data.length}`}>
                                <Pie
                                    dataKey="count"
                                    isAnimationActive={false}
                                    data={vis3Data}
                                    cx="50%"
                                    cy="50%"
                                    outerRadius={200}
                                    fill="#8884d8"
                                    label
                                />
                                <Tooltip />
                            </PieChart>
                        </ResponsiveContainer>
                    )}
            </Container>
        </div>
        );
    }
    export default ProviderVisualizations;
        