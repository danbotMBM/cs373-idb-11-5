import React, { useState, useEffect } from "react";
import axios from 'axios';
import { Row, Container, Card, CardBody, CardImg, Button, Input, Alert, Pagination, PaginationItem, PaginationLink } from 'reactstrap';
import { ScatterChart, Scatter, BarChart, Bar, Legend, XAxis, YAxis, CartesianGrid, Tooltip, ResponsiveContainer } from 'recharts';

// Code derived from https://gitlab.com/catchingcongress/catchingcongress/-/blob/main/frontend/src/pages/committees/CommitteeInstance.tsx
function numberWithCommas(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',');
}

function Visualizations() {

    const countriesAPI = `https://api.soccerstars.me/countries`;
    const teamsAPI = `https://api.soccerstars.me/teams`;
    const athletesAPI = `https://api.soccerstars.me/athletes`;

    const [loading, setLoading] = useState(false);

    const [vis1Data, setVis1Data] = useState([]);
    const [vis2Data, setVis2Data] = useState([]);
    const [vis3Data, setVis3Data] = useState([]);

    const [vis1Page, setVis1Page] = useState(1);

    const countryAthletes = new Map();
    const countryPop = new Map();
    const countryRegion = new Map();
    const regionAthletes = new Map();

    useEffect(() => {
        document.title = `Soccer Stars | Visualizations`;
    });

    useEffect(() => {
        setLoading(true);
        axios.get(countriesAPI, {
            params: {
                per_page: 1000000
            }
        })
            .then((response) => {
                for (let i = 0; i < response.data.count; i++) {
                    countryAthletes.set(response.data.data[i].name, 0);
                    countryPop.set(response.data.data[i].name, response.data.data[i].population);
                    countryRegion.set(response.data.data[i].name, response.data.data[i].region);
                    regionAthletes.set(response.data.data[i].region, 0);
                }
            })
            .catch(function (error) {
                console.error(error);
            })
            .then(function () {
                axios.get(teamsAPI, {
                    params: {
                        per_page: 1000000
                    }
                })
                    .then((response) => {
                        const data = [];
                        for (let i = 0; i < response.data.count; i++) {
                            if (response.data.data[i].date_founded > 1000) {
                                data.push({date_founded: response.data.data[i].date_founded,
                                    win_percent: response.data.data[i].total_games == 0 ? "0" :
                                    Math.round(100 * (response.data.data[i].wins / response.data.data[i].total_games))});
                            }
                        }
                        setVis2Data(data);
                    })
                    .catch(function (error) {
                        console.error(error);
                    })
                    .then(function () {
                        axios.get(athletesAPI, {
                            params: {
                                per_page: 1000000
                            }
                        })
                            .then((response) => {
                                for (let i = 0; i < response.data.count; i++) {
                                    countryAthletes.set(response.data.data[i].country, countryAthletes.get(response.data.data[i].country) + 1);
                                    regionAthletes.set(countryRegion.get(response.data.data[i].country),
                                        regionAthletes.get(countryRegion.get(response.data.data[i].country)) + 1);
                                }
                            })
                            .catch(function (error) {
                                console.error(error);
                            })
                            .then(function () {
                                let data = [];
                                for (const x of countryAthletes.keys()) {
                                    data.push({name: x, athletes: countryAthletes.get(x), population: countryPop.get(x)});
                                }
                                setVis1Data(data);
                                data = [];
                                for (const x of regionAthletes.keys()) {
                                    data.push({region: x, athletes: regionAthletes.get(x)});
                                }
                                setVis3Data(data);
                                console.log(data);
                                setLoading(false);
                            });
                    });
            });
    }, [countriesAPI, teamsAPI, athletesAPI]);
    
    return (
        <div>
            <Container className="py-5">
                <h1 className="pb-3">Visualizations</h1>
            </Container>
            <Container className="mb-3">
                <h2 className="mb-3">Athletes vs Population</h2>
                <p>Is soccer is more valuable to the cultures of some countries than to others?</p>
                    { loading ? (
                        <Alert color="warning" className="mb-0">Loading data...</Alert>
                    ) : (
                        <div>
                            <Pagination className="d-flex justify-content-center">
                                <PaginationItem disabled={vis1Page == 1}>
                                    <PaginationLink
                                        first
                                        onClick={() => setVis1Page(1)}
                                    />
                                </PaginationItem>
                                <PaginationItem disabled={vis1Page == 1}>
                                    <PaginationLink
                                        previous
                                        onClick={() => setVis1Page(vis1Page - 1)}
                                    />
                                </PaginationItem>
                                <PaginationItem>
                                    <PaginationLink>
                                        {vis1Page}
                                    </PaginationLink>
                                </PaginationItem>
                                <PaginationItem disabled={vis1Page == Math.floor(vis1Data.length / 10) + 1}>
                                    <PaginationLink
                                        next
                                        onClick={() => setVis1Page(vis1Page + 1)}
                                    />
                                </PaginationItem>
                                <PaginationItem disabled={vis1Page == Math.floor(vis1Data.length / 10) + 1}>
                                    <PaginationLink
                                        last
                                        onClick={() => setVis1Page(Math.floor(vis1Data.length / 10) + 1)}
                                    />
                                </PaginationItem>
                            </Pagination>
                            <ResponsiveContainer width="100%" height={600} key={`rc_${vis1Data.length}`}>
                                <BarChart
                                    key={`bc_${vis1Data.length}`}
                                    data={vis1Data.slice(1 + (10 * (vis1Page - 1)), Math.min(1 + (10 * vis1Page), vis1Data.length))}
                                    margin={{
                                        top: 50,
                                        right: 50,
                                        bottom: 50,
                                        left: 50
                                    }}
                                >
                                    <CartesianGrid strokeDasharray="3 3" />
                                    <XAxis key={`x_${vis1Data.length}`} dataKey="name" name="Country" />
                                    <YAxis key={`y1_${vis1Data.length}`} yAxisId="left" orientation="left" stroke="#8884d8" />
                                    <YAxis key={`y2_${vis1Data.length}`} yAxisId="right" orientation="right" stroke="#82ca9d" />
                                    <Tooltip />
                                    <Legend />
                                    <Bar yAxisId="left" dataKey="athletes" name="Athletes" fill="#8884d8" />
                                    <Bar yAxisId="right" dataKey="population" name="Population" fill="#82ca9d" />
                                </BarChart>
                            </ResponsiveContainer>
                        </div>
                    )}
            </Container>
            <Container className="mb-3">
                <h2 className="mb-3">Date Founded vs Win Percentage</h2>
                <p>Do teams perform better when they've been established for longer?</p>
                    { loading ? (
                        <Alert color="warning" className="mb-0">Loading data...</Alert>
                    ) : (
                        <ResponsiveContainer width="100%" height={600} key={`rc_${vis2Data.length}`}>
                            <ScatterChart
                                key={`sc_${vis2Data.length}`}
                                margin={{
                                    top: 50,
                                    right: 50,
                                    bottom: 50,
                                    left: 50
                                }}
                            >
                                <CartesianGrid />
                                <XAxis key={`x_${vis2Data.length}`} type="number" dataKey="date_founded" name="Date Founded" domain={["dataMin", "dataMax"]} />
                                <YAxis key={`y_${vis2Data.length}`} type="number" dataKey="win_percent" name="Win Percentage" unit="%" />
                                <Tooltip cursor={{ strokeDasharray: '3 3' }} />
                                <Scatter data={vis2Data} fill="#8884d8" />
                            </ScatterChart>
                        </ResponsiveContainer>
                    )}
            </Container>
            <Container className="mb-3">
                <h2 className="mb-3">Number of Athletes by Region</h2>
                <p>Does the number of athletes signify the value of soccer in some regions compared to others?</p>
                    { loading ? (
                        <Alert color="warning" className="mb-0">Loading data...</Alert>
                    ) : (
                        <ResponsiveContainer width="100%" height={600} key={`rc_${vis3Data.length}`}>
                            <BarChart
                                key={`bc_${vis3Data.length}`}
                                data={vis3Data}
                                margin={{
                                    top: 50,
                                    right: 50,
                                    bottom: 50,
                                    left: 50
                                }}
                            >
                                <CartesianGrid strokeDasharray="3 3" />
                                <XAxis key={`x_${vis3Data.length}`} dataKey="region" name="Region" />
                                <YAxis key={`y_${vis3Data.length}`} name="Athletes" />
                                <Tooltip />
                                <Bar dataKey="athletes" name="Athletes" fill="#8884d8" />
                            </BarChart>
                        </ResponsiveContainer>
                    )}
            </Container>
        </div>
    );
}
export default Visualizations;
