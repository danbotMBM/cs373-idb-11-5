# cs373-idb-11-5

# Website
https://www.soccerstars.me/

# Develop url
https://develop.d36n1ulz5li55j.amplifyapp.com/

## Canvas group
11 AM, Group 5

## Names
Daniel Jones
Yazan Alsukhni
Grant Shirah
Dhruv Sethi

## Project Name
soccerstars.me

## URL
https://gitlab.com/danbotMBM/cs373-idb-11-5

## Proposal
Soccer Stars aims to promote engagement with soccer on a global scale, relate teams to the events and successes of their respective countries, and expand recognition of global cultures.

### API urls
https://restcountries.com/

https://www.api-football.com/

https://newsdata.io/

### Models
1. Countries (> 250)
2. Teams (> 1000)
3. Athletes (> 5000)

### 5 attributes of each model for filtering/sorting
* Countries
    * name
    * population
    * latitude
    * longitude
    * area

* Teams
    * number of trophies
    * country
    * number of games
    * wins
    * losses
    * goals
    * goals avg
    * goals against
    * name
    
* Athletes
    * number
    * first name
    * last name
    * age
    * height
    * weight
    * goals
    * penalties

### 5 additional attributes for searching
* Countries
    * capital
    * region
    * timezones
    * bordering countries
    * native name
    * currencies
    * languages

* Teams
    * league
    * venue name
    * venue surfate
    * venue city
    * venue capacity
    * venue address
    * date founded

* Athletes
    * Teams
    * Country
    * birth place
    * position
    * nationality

### Types of media for each model
* Countries
    * maps
    * image of flag
    * images of landmarks
    * news articles

* Teams
    * image of logo
    * images of home field/stadium
    * latest game outcome

* Athletes
    * images of athlete
    * image of current team logo
    * news articles

### 3 questions for data synthesis
1. What cultures/countries are historically underrepresented in the sport?
2. What are the cultural backgrounds of my favorite soccer players?
3. What current events are happening in the countries of the soccer teams I watch on TV?